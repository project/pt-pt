# LANGUAGE translation of Drupal (modules/taxonomy.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
msgid ""
msgstr ""
"Project-Id-Version: Drupal pt-pt 4.7\n"
"POT-Creation-Date: 2004-08-25 18:42+0200\n"
"PO-Revision-Date: 2006-12-24 15:32-0000\n"
"Last-Translator: Miguel Duarte <home@miguelduarte.net>\n"
"Language-Team: Portuguese from Portugal <home@miguelduarte.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: Portuguese\n"
"X-Poedit-Country: PORTUGAL\n"
"X-Poedit-SourceCharset: utf-8\n"

#: modules/legacy/legacy.module:42
#: ;47 modules/taxonomy/taxonomy.info:0
#, fuzzy
msgid "Taxonomy"
msgstr "taxonomia"

#: modules/taxonomy/taxonomy.module:67
#, fuzzy
msgid "Create vocabularies and terms to categorize your content."
msgstr "Activada a organização do conteúdo por categorias."

#: modules/taxonomy/taxonomy.module:77
#, fuzzy
msgid "Add vocabulary"
msgstr "adicionar vocabulário"

#: modules/taxonomy/taxonomy.module:84
#, fuzzy
msgid "Edit vocabulary"
msgstr "editar vocabulário"

#: modules/taxonomy/taxonomy.module:90
#, fuzzy
msgid "Edit term"
msgstr "editar termo"

#: modules/taxonomy/taxonomy.module:96
#, fuzzy
msgid "Taxonomy term"
msgstr "administrar taxonomia"

#: modules/taxonomy/taxonomy.module:102
#, fuzzy
msgid "Autocomplete taxonomy"
msgstr "administrar taxonomia"

#: modules/taxonomy/taxonomy.module:111
#, fuzzy
msgid "List terms"
msgstr "Termos pai"

#: modules/taxonomy/taxonomy.module:123
#, fuzzy
msgid "Add term"
msgstr "adicionar termo"

#: modules/taxonomy/taxonomy.module:148
msgid "edit vocabulary"
msgstr "editar vocabulário"

#: modules/taxonomy/taxonomy.module:149
msgid "list terms"
msgstr "lista de termos"

#: modules/taxonomy/taxonomy.module:150
#, fuzzy
msgid "add terms"
msgstr "Termos pai"

#: modules/taxonomy/taxonomy.module:154
msgid "No categories available."
msgstr "Nenhuma categoria disponível."

#: modules/taxonomy/taxonomy.module:189
msgid "No terms available."
msgstr "Não há termos disponíveis."

#: modules/taxonomy/taxonomy.module:207
msgid "Vocabulary name"
msgstr "Nome do vocabulário"

#: modules/taxonomy/taxonomy.module:210
#, fuzzy
msgid "The name for this vocabulary. Example: \"Topic\"."
msgstr "O nome para este vocabulário. Exemplo: \"Tópico\"."

#: modules/taxonomy/taxonomy.module:216
msgid "Description of the vocabulary; can be used by modules."
msgstr "Descrição do vocabulário; pode ser utilizada pelos módulos."

#: modules/taxonomy/taxonomy.module:219
msgid "Help text"
msgstr "Texto de ajuda"

#: modules/taxonomy/taxonomy.module:221
msgid "Instructions to present to the user when choosing a term."
msgstr "Instruções a exibir ao utilizador quando este estiver a escolher um termo."

#: modules/taxonomy/taxonomy.module:224
msgid "Types"
msgstr "Tipos"

#: modules/taxonomy/taxonomy.module:227
msgid "A list of node types you want to associate with this vocabulary."
msgstr "Uma lista de tipos de nós que deseja associar a este vocabulário."

#: modules/taxonomy/taxonomy.module:231
msgid "Hierarchy"
msgstr "Hierarquia"

#: modules/taxonomy/taxonomy.module:233
msgid "Single"
msgstr "Simples"

#: modules/taxonomy/taxonomy.module:233
msgid "Multiple"
msgstr "Múltiplo"

#: modules/taxonomy/taxonomy.module:234
#, fuzzy
msgid "Allows <a href=\"@help-url\">a tree-like hierarchy</a> between terms of this vocabulary."
msgstr "Permitir <a href=\"%help-url\">uma hierarquia em árvore</a> entre os termos deste vocabulário."

#: modules/taxonomy/taxonomy.module:237
#: ;382
msgid "Related terms"
msgstr "Termos relacionados"

#: modules/taxonomy/taxonomy.module:239
#, fuzzy
msgid "Allows <a href=\"@help-url\">related terms</a> in this vocabulary."
msgstr "Permitir <a href=\"%help-url\">termos relacionados</a> neste vocabulário."

#: modules/taxonomy/taxonomy.module:242
msgid "Free tagging"
msgstr "Etiquetagem livre"

#: modules/taxonomy/taxonomy.module:244
msgid "Content is categorized by typing terms instead of choosing from a list."
msgstr "O conteúdo é categorizado escrevendo termos em vez de escolhendo-os a partir de uma lista."

#: modules/taxonomy/taxonomy.module:247
msgid "Multiple select"
msgstr "Selecção múltipla."

#: modules/taxonomy/taxonomy.module:249
msgid "Allows nodes to have more than one term from this vocabulary (always true for free tagging)."
msgstr "Permite que os nós tenham mais que um termo neste vocabulário (verdade sempre que livre de marcas \"TAGS\")."

#: modules/taxonomy/taxonomy.module:254
msgid "If enabled, every node <strong>must</strong> have at least one term in this vocabulary."
msgstr "Se activado, todos os nós <strong>terão que ter</strong> pelo menos um termos neste vocabulário."

#: modules/taxonomy/taxonomy.module:259
msgid "In listings, the heavier vocabularies will sink and the lighter vocabularies will be positioned nearer the top."
msgstr "Nas listagens, os vocabulários mais pesados irão afundar e os vocabulários mais leves irão ficar colocados junto ao topo."

#: modules/taxonomy/taxonomy.module:279
msgid "Created new vocabulary %name."
msgstr "Criado o novo vocabulário \"%name\"."

#: modules/taxonomy/taxonomy.module:282
msgid "Updated vocabulary %name."
msgstr "Actualizado o vocabulário \"%name\"."

#: modules/taxonomy/taxonomy.module:342
msgid "Are you sure you want to delete the vocabulary %title?"
msgstr "Tem a certeza que pretende eliminar o comentário %title?"

#: modules/taxonomy/taxonomy.module:345
msgid "Deleting a vocabulary will delete all the terms in it. This action cannot be undone."
msgstr "Eliminando um vocabulário irá eliminar todos os seus termos. Esta ação não pode ser desfeita."

#: modules/taxonomy/taxonomy.module:352
msgid "Deleted vocabulary %name."
msgstr "Removido vocabulário \"%name\"."

#: modules/taxonomy/taxonomy.module:359
msgid "Term name"
msgstr "Nome do termo"

#: modules/taxonomy/taxonomy.module:359
#, fuzzy
msgid "The name for this term. Example: \"Linux\"."
msgstr "O nome para este termo. Exemplo: \"Linux\"."

#: modules/taxonomy/taxonomy.module:361
msgid "A description of the term."
msgstr "Uma descrição do termo."

#: modules/taxonomy/taxonomy.module:374
msgid "Parent term"
msgstr "Termo pai"

#: modules/taxonomy/taxonomy.module:377
msgid "Parents"
msgstr "Pais"

#: modules/taxonomy/taxonomy.module:377
msgid "Parent terms"
msgstr "Termos pai"

#: modules/taxonomy/taxonomy.module:385
msgid "Synonyms"
msgstr "Sinónimos"

#: modules/taxonomy/taxonomy.module:385
#, fuzzy
msgid "<a href=\"@help-url\">Synonyms</a> of this term, one synonym per line."
msgstr "Os <a href=\"%help-url\">sinónimos</a> deste termo (um por linha)."

#: modules/taxonomy/taxonomy.module:386
msgid "In listings, the heavier terms will sink and the lighter terms will be positioned nearer the top."
msgstr "Nas listagens, os termos mais pesados irão afundar e os termos mais leves irão ficar colocados junto ao topo."

#: modules/taxonomy/taxonomy.module:407
msgid "Created new term %term."
msgstr "Criar um novo termo com a denominação de %term."

#: modules/taxonomy/taxonomy.module:410
msgid "The term %term has been updated."
msgstr "O termo %term foi actualizado."

#: modules/taxonomy/taxonomy.module:521
msgid "Are you sure you want to delete the term %title?"
msgstr "Tem a certeza que pretende eliminar o termo %title?"

#: modules/taxonomy/taxonomy.module:524
msgid "Deleting a term will delete all its children if there are any. This action cannot be undone."
msgstr "Eliminando um termo irá eliminar todos os seus filhos. Esta ação não pode ser desfeita."

#: modules/taxonomy/taxonomy.module:531
msgid "Deleted term %name."
msgstr "Eliminado o termo %name."

#: modules/taxonomy/taxonomy.module:639
#, fuzzy
msgid "A comma-separated list of terms describing this content. Example: funny, bungee jumping, \"Company, Inc.\"."
msgstr "Uma lista de termos separados por vírgulas que descrevem este conteúdo (Exemplo: engraçado, desportos radicais, \"Empresa, S.A.\")."

#: modules/taxonomy/taxonomy.module:719
msgid "The %name vocabulary can not be modified in this way."
msgstr "O vocabulário %name não pode ser modificado de esta forma."

#: modules/taxonomy/taxonomy.module:1203
msgid "There are currently no posts in this category."
msgstr "Não existe conteúdo nesta categoria."

#: modules/taxonomy/taxonomy.module:1377
msgid "The taxonomy module is one of the most popular features because users often want to create categories to organize content by type. It can automatically classify new content, which is very useful for organizing content on-the-fly. A simple example would be organizing a list of music reviews by musical genre."
msgstr "O módulo taxonomy é uma das funcionalidades mais populares porque os utilizadores frequentemente querem criar categorias para organizar o conteúdo por tipos. Pode automaticamente classificar conteúdos novos, o que é muito útil para organizar o conteúdo dinamicamente. Um exemplo simples poderia ser  organizar uma lista de comentários sobre o tema música pelo género musical."

#: modules/taxonomy/taxonomy.module:1378
msgid "Taxonomy is also the study of classification. The taxonomy module allows you to define vocabularies (sets of categories) which are used to classify content. The module supports hierarchical classification and association between terms, allowing for truly flexible information retrieval and classification. The taxonomy module allows multiple lists of categories for classification (controlled vocabularies) and offers the possibility of creating thesauri (controlled vocabularies that indicate the relationship of terms) and taxonomies (controlled vocabularies where relationships are indicated hierarchically). To view and manage the terms of each vocabulary, click on the associated <em>list terms</em> link. To delete a vocabulary and all its terms, choose <em>edit vocabulary.</em>"
msgstr "<p>O módulo taxonomy permite-lhe classificar o conteúdo em categorias e sub-categorias; permite-lhe listas múltiplas de categorias por classificação (vocabulários controlados) e oferecer a possibilidade de criar um dicionário de sinónimos (vocabulários controlados onde as relações são indicadas hierarquicamente). Para apagar um termo escolha \"editar termo\". Para apagar um vocabulário e todos os seus termos, escolha \"editar vocabulário\".</p>"

#: modules/taxonomy/taxonomy.module:1379
msgid "A controlled vocabulary is a set of terms to use for describing content (known as descriptors in indexing lingo). Drupal allows you to describe each piece of content (blog, story, etc.) using one or many of these terms. For simple implementations, you might create a set of categories without subcategories, similar to Slashdot's sections. For more complex implementations, you might create a hierarchical list of categories."
msgstr "Quando cria um vocabulário controlado está a criar um conjunto de termos utilizados para descrever conteúdos (mais conhecidos como descritores na linguagem de arquivo). O Drupal permite-lhe descrever cada conteúdo (artigo, artigo de blog, etc.), utilizando um ou mais destes termos. Para implementações simples poderá criar um conjunto de categorias sem sub-categorias, à semelhança das secções do Slashdot.org ou do Kuro5hin.org. Para implementações mais complexas poderá organizar o conteúdo sob a forma de uma lista hierárquica de categorias."

#: modules/taxonomy/taxonomy.module:1380
#, fuzzy
msgid "For more information please read the configuration and customization handbook <a href=\"@taxonomy\">Taxonomy page</a>."
msgstr "Para informações adicionais consulte o manual de configuração e personalização <a href=\"%taxonomy\">Página de Taxonomia</a>."

#: modules/taxonomy/taxonomy.module:1383
#, fuzzy
msgid "The taxonomy module allows you to classify content into categories and subcategories; it allows multiple lists of categories for classification (controlled vocabularies) and offers the possibility of creating thesauri (controlled vocabularies that indicate the relationship of terms), taxonomies (controlled vocabularies where relationships are indicated hierarchically), and free vocabularies where terms, or tags, are defined during content creation. To view and manage the terms of each vocabulary, click on the associated <em>list terms</em> link. To delete a vocabulary and all its terms, choose \"edit vocabulary\"."
msgstr "<p>O módulo de taxonomia permite-lhe classificar o conteúdo em categorias e sub-categorias; permite-lhe listas múltiplas de categorias por classificação (vocabulários controlados) e oferecer a possibilidade de criar um dicionário de sinónimos (vocabulários controlados onde as relações são indicadas hierarquicamente). Para apagar um termo escolha \"editar termo\". Para apagar um vocabulário e todos os seus termos, escolha \"editar vocabulário\".</p>"

#: modules/taxonomy/taxonomy.module:1385
#, fuzzy
msgid "When you create a controlled vocabulary you are creating a set of terms to use for describing content (known as descriptors in indexing lingo). Drupal allows you to describe each piece of content (blog, story, etc.) using one or many of these terms. For simple implementations, you might create a set of categories without subcategories, similar to Slashdot.org's or Kuro5hin.org's sections. For more complex implementations, you might create a hierarchical list of categories."
msgstr "Quando cria um vocabulário contrulado está a criar um conjunto de termos utilizados para descrever conteúdos (mais conhecidos como descritores na linguagem de arquivo). O Drupal permite-lhe descrever cada conteúdo (artigo, artigo de blog, etc.), utilizando um ou mais destes termos. Para implementações simples poderá criar um conjunto de categorias sem sub-categorias, à semelhança das secções do Slashdot.org ou do Kuro5hin.org. Para implementações mais complexas poderá organizar o conteúdo sob a forma de uma lista hierárquica de categorias."

#: modules/taxonomy/taxonomy.module:13
msgid "administer taxonomy"
msgstr "administrar taxonomia"

#: modules/taxonomy/taxonomy.module:0
#, fuzzy
msgid "taxonomy"
msgstr "taxonomia"

#: modules/taxonomy/taxonomy.info:0
msgid "Enables the categorization of content."
msgstr "Activada a organização do conteúdo por categorias."

