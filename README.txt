﻿Este directório contém a tradução para Português (Portugal) dos módulos base do
Drupal. Caso tenha alguma sugestão a fazer, ou pretenda participar na equipa de
tradução, poderá faze-lo através dos seguintes endereços:

Bug Tracker
  http://drupal.org/project/issues/pt-pt

Lista de Discussão
  http://groups.google.com/group/drupal-portugal

Grupo de Utilizadores de Portugal
  http://groups.drupal.org/og/users/956


Quem contribuiu para a tradução pt-pt 5.0
=========================================
Miguel Cunha Duarte home@miguelduarte.net
Raul Pedro Santos borfast@gmail.com
Fernando Silva fernando.silva@openquest.pt
